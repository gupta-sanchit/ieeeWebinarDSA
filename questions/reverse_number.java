import java.util.Scanner;

public class reverse_number {
    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        int reverse = 0;

        while(n!=0)
        {
            int last_digit = n % 10;
            reverse = reverse*10 + last_digit;
            n = n/10;
        }
        System.out.println(reverse);
    }
}