import java.util.Scanner;

public class print_digits {
    public static void main(String[] args){
        java.util.Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();

        while(n!=0)
        {
            int last_digit = n % 10;
            System.out.println(last_digit);
            n = n/10;
        }
    }
    
}