import java.util.Scanner;

public class inverse {
    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        int pos = 1;
        int inverse = 0;

        while(n!=0)
        {
            int last_digit = n % 10;
            inverse = inverse + pos * (int)Math.pow(10, last_digit - 1);
            pos++;
            n = n/10;
        }
        System.out.println(inverse);
    }
    
}