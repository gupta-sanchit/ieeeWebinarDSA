public class variables{

    public static void main(String[] args){
        int x = 10;
        int y = 20;

        int sum = x + y;

        System.out.println("Sum of x & y is: " + sum); 

        // ==> product

        int product = x * y;
        System.out.println("Product of x & y is: " + product);


        int var_1 = x / y;
        int var_2 = y / x;
        int remainder = x % y;

        System.out.println(var_1);
        System.out.println(var_2);
        System.out.println(remainder);

        int expression = (x + y) * (x - y)/(x*y);
        System.out.println("Expression: " + expression);


    }
}