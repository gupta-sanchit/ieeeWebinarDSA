public class conditional {
    public static void main(String[] args){

        int x = 11;

        if (x % 2 == 0)
        {
            System.out.println("X is Even");
        }
        else
        {
            System.out.println("X is odd");
        }

        int x1 = 1000;
        int x2 = 10000;

        if (x1 == x2)
        {
            System.out.println(x1 + " is Equal to " + x2);
        }
        else
        {
            if(x1 > x2)
            {
                System.out.println(x1 + " greater than " + x2);
            }
            else
            {
                System.out.println(x1 + " is less than " + x2);
            }
        }
    }
    
}